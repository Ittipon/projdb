@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

            </div>

            <div class="panel-body">
            <a  class="btn btn-success" href="{{ route('employee.create') }}">เพิ่ม</a>
            
            <table class="table">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">surName</th>
      <th scope="col">img</th>
      <th scope="col">sex</th>
      <th scope="col">phone</th>
      <th scope="col">email</th>
      <th scope="col">birthday</th>
      <th scope="col">nationality</th>
      <th scope="col">qualification</th>
      
      <th scope="col">position</th>
      <th scope="col">department</th>
      <th scope="col" colspan=2>Option</th>
    </tr>
  </thead>
  <?php $i=1; ?>
  @foreach($employee as $row)
  <tbody>
    

      <th scope="col">{{ $row->id }}</th>
      <th scope="col">{{ $row->emp_name }}</th>
      <th scope="col">{{ $row->emp_surName }}</th>
      <th scope="col">{{ $row->emp_img }}</th>
      <th scope="col">{{ $row->emp_sex }}</th>
      <th scope="col">{{ $row->emp_phone }}</th>
      <th scope="col">{{ $row->emp_email }}</th>
      <th scope="col">{{ $row->emp_birthday }}</th>
      <th scope="col">{{ $row->emp_nationality }}</th>
      <th scope="col">{{ $row->emp_qualification }}</th>

      <th scope="col">{{ $row->pos_name }}</th>
      <th scope="col">{{ $row->dep_name }}</th>
      <th><a class="btn btn-warning" href="{{ route('employee.edit',$row->id) }}">edit</a></th>
      
      <th><form action="{{ route('employee.destroy',$row->id) }}" method ="post"> 
          @csrf 
          @method("DELETE")
          <button class="btn btn-danger">Delete</button>
        </form>
      </th>    
  </tbody>
 @endforeach 
</table>

            </div>
        </div>
    </div>
</div>
@endsection