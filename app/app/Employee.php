<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = "Employee";

    protected $fillable = ['id','emp_name','emp_surName','emp_img','emp_sex','emp_phone','emp_email','emp_birthday','emp_nationality',
    'emp_qualification','position_pos_id'];
}
