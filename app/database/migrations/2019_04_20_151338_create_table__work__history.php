<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWorkHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Work_History', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('his_nameOrg',100);
            $table->char('his_position',45);
            $table->date('his_start');
            $table->date('his_end');
            $table->unsignedBigInteger('Employee_emp_id');
            $table->foreign('Employee_emp_id')->references('id')->on('Employee')->primary();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Work_History');
    }
}
