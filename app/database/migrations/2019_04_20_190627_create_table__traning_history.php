<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTraningHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Traning_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('tran_course',100);
            $table->char('tran_addr',100);
            $table->date('tran_date');
            $table->char('tran_type',45);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Traning_history');
    }
}
